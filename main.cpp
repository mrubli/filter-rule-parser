#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "Filter.h"

using namespace std;
using namespace logitech::uvcsnoop::filter;


class TestFilterEnvironment
	: public IFilterEnvironment
{
public:
	TestFilterEnvironment()
	{
		symbols_["int"] = make_shared<Symbol>("int", Type::IntType());
		symbols_["uint"] = make_shared<Symbol>("uint", Type::UIntType());
		symbols_["double"] = make_shared<Symbol>("double", Type::DoubleType());
		symbols_["string"] = make_shared<Symbol>("string", Type::StringType());
		symbols_["raw"] = make_shared<Symbol>("raw", Type::RawType());

		values_["int"] = -9;
		values_["uint"] = 42u;
		values_["double"] = 3.14;
		values_["string"] = "foo";
		values_["raw"] = std::vector<uint8_t>();
	}

public:
	SymbolPtr		LookupVariable(const std::string& name)
	{
		SymbolPtr symbol;

		auto lookup = symbols_.find(name);
		if(lookup != symbols_.end())
		{
			symbol = symbols_.at(name);
		}

		return symbol;
	}

	boost::optional<ValType>	GetVariableValue(const SymbolPtr& symbol)
	{
		auto lookup = values_.at(symbol->Name);
		return lookup;
	}

private:
	std::map<std::string, SymbolPtr>	symbols_;
	std::map<std::string, ValType>	values_;
};

IFilterEnvironmentPtr Env = make_shared<TestFilterEnvironment>();


void testParser()
{
	vector<string> testStrings;

	// Identifiers and constants
	testStrings.push_back(" foo=0");
	testStrings.push_back("foo=0 ");
	testStrings.push_back(" foo=0 ");
	testStrings.push_back(" foo = 0 ");
	testStrings.push_back("foo =0");
	testStrings.push_back("foo= 0");
	testStrings.push_back("foo = 0");
	testStrings.push_back("foo.bar=0");
	testStrings.push_back("foo.bar = 0");
	testStrings.push_back("foo.bar.boo=0");
	testStrings.push_back("FOO=0");
	testStrings.push_back("foo=42");
	testStrings.push_back("foo=-42");
	testStrings.push_back("foo=\"\"");
	testStrings.push_back("foo=\"bar\"");
	testStrings.push_back("foo=0x0");
	testStrings.push_back("foo=0x1234");
	testStrings.push_back("foo=0xDEADBEEF");
	testStrings.push_back("foo=0xdeadbeef");
	testStrings.push_back("foo=3.14");
	testStrings.push_back("foo=3.");
	testStrings.push_back("foo=3.141592653589793");
	testStrings.push_back("bytes={}");
	testStrings.push_back("bytes={00}");
	testStrings.push_back("bytes={ 00 11 22 33 44 55 aa bb cc }");

	// Comparison operators
	testStrings.push_back("foo!=42");
	testStrings.push_back("foo<42");
	testStrings.push_back("foo>42");
	testStrings.push_back("foo<=42");
	testStrings.push_back("foo>=42");
	testStrings.push_back("foo!=-42");
	testStrings.push_back("foo<-42");
	testStrings.push_back("foo>-42");
	testStrings.push_back("foo<=-42");
	testStrings.push_back("foo>=-42");

	// Expressions
	testStrings.push_back("(foo=42)");
	testStrings.push_back("not (foo=42)");
	testStrings.push_back("(not foo=42)");
	testStrings.push_back("not foo=42");
	testStrings.push_back("foo=42 or bar=12");
	testStrings.push_back("foo=42 and bar=12");
	testStrings.push_back("foo=42 and bar=\"abc\"");
	testStrings.push_back("foo=42 and bar=12 or boo=98");
	testStrings.push_back("foo=42 or bar=12 and boo=98");
	testStrings.push_back("(foo=42 or bar=12)");
	testStrings.push_back("(foo=42 and bar=12)");
	testStrings.push_back("(foo=42 or not bar=12)");
	testStrings.push_back("(foo=42 and not bar=12)");
	testStrings.push_back("(not foo=42 or bar=12)");
	testStrings.push_back("(not foo=42 and bar=12)");
	testStrings.push_back("not (foo=42 or bar=12)");
	testStrings.push_back("not (foo=42 and bar=12)");
	testStrings.push_back("not foo=42 and not bar=6");
	testStrings.push_back("a=1 and b!=2 and c<3");
	testStrings.push_back("a>1 and b<=2 or c>=3 and d=4");

	FilterRuleParser filterRuleParser(Env);

	unsigned int failures = 0;
	for(const auto& input : testStrings)
	{
		auto rule = filterRuleParser.Parse(input);
		bool success = rule->IsSyntaxValid();

		if(!success)
			failures++;

		cout << "Input:  " << input << endl;
		cout << "Result: " << (success ? "OK" : "FAILED") << endl;
		cout << "Output: " << rule->ToString() << endl;
		cout << endl;
	}

	cout << testStrings.size() << " parser tests, " << failures << " failure(s)." << endl;
}


void checkerTest()
{
	vector<string> testStrings;

	testStrings.push_back("int=0");
	testStrings.push_back("uint=0x0");
	testStrings.push_back("double=0.");
	testStrings.push_back("raw={}");
	testStrings.push_back("string=\"\"");

	testStrings.push_back("not int=0");
	testStrings.push_back("not uint=0x0");
	testStrings.push_back("not double=0.");
	testStrings.push_back("not raw={}");
	testStrings.push_back("not string=\"\"");

	testStrings.push_back("int=0.");

	FilterRuleParser filterRuleParser(Env);

	unsigned int failures = 0;
	for(const auto& input : testStrings)
	{
		auto rule = filterRuleParser.Parse(input);
		bool success = rule->IsValid();

		if(!success)
			failures++;

		cout << "Input:  " << input << endl;
		cout << "Result: " << (success ? "OK" : "FAILED") << endl;
		cout << "Output: " << rule->ToString(Rule::PrintTypes) << endl;
		cout << endl;
	}

	cout << testStrings.size() << " checker tests, " << failures << " failure(s)." << endl;
}


void evaluatorTest()
{
	vector<string> testStrings;

	testStrings.push_back("int=-9");
	testStrings.push_back("uint=0x2a");
	testStrings.push_back("double=3.14");
	testStrings.push_back("raw={}");
	testStrings.push_back("string=\"foo\"");

	testStrings.push_back("not int=2");
	testStrings.push_back("not uint=0xa");
	testStrings.push_back("not double=0.");
	testStrings.push_back("not raw={ de ad be ef }");
	testStrings.push_back("not string=\"hello world\"");

	FilterRuleParser filterRuleParser(Env);

	unsigned int failures = 0;
	for(const auto& input : testStrings)
	{
		auto rule = filterRuleParser.Parse(input);
		bool success = rule->Match();

		if(!success)
			failures++;

		cout << "Input:  " << input << endl;
		cout << "Result: " << (success ? "TRUE" : "FALSE") << endl;
		cout << "Output: " << rule->ToString(Rule::PrintValues) << endl;
		cout << endl;
	}

	cout << testStrings.size() << " evaluator tests, " << failures << " failure(s)." << endl;
}


int main()
{
	testParser();
	cout << endl;
	checkerTest();
	cout << endl;
	evaluatorTest();
	
	return 0;
}
