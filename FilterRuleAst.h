#pragma once

#include <memory>
#include <ostream>
#include <string>
#include <vector>

#include "visitor.h"

#include "Filter.h"


namespace logitech
{
namespace uvcsnoop
{
namespace filter
{
namespace impl
{


class AstNode
	: public util::BaseVisitable<>
{
protected:
	AstNode()
		: type_()
	{}
public:
	virtual ~AstNode() {}
	DEFINE_VISITABLE()

	TypePtr					type_;
	boost::optional<bool>	result_;

	friend std::ostream& operator<<(std::ostream& os, const AstNode& e);
};

typedef std::shared_ptr<AstNode> AstNodePtr;


template <typename E>
AstNodePtr make_ast_node_ptr(E const& t)
{
	return make_shared<E>(t);
}


class VarRef
	: public AstNode
{
public:
	VarRef() {}
	VarRef(const std::string& name) : name_(name) {}
	DEFINE_VISITABLE()

	std::string		name_;
	SymbolPtr		symbol_;
};


class ConstVal
	: public AstNode
{
private:
	struct ValuePrinter;

public:
	ConstVal() {}
	ConstVal(int value) : value_(value) {}
	ConstVal(unsigned int value) : value_(value) {}
	ConstVal(double value) : value_(value) {}
	ConstVal(const std::string& value) : value_(value) {}
	ConstVal(std::vector<uint8_t> value) : value_(value) {}
	DEFINE_VISITABLE()

	ValType value_;
};


class BinOp
	: public AstNode
{
public:
	template<typename L, typename R>
	BinOp(const std::string& op, const L& lhs, const R& rhs)
		: op_(op), lhs_(make_ast_node_ptr(lhs)), rhs_(make_ast_node_ptr(rhs))
	{}
	DEFINE_VISITABLE()

	std::string		op_;
	AstNodePtr		lhs_;
	AstNodePtr		rhs_;
};


class UnOp
	: public AstNode
{
public:
	template<typename E>
	UnOp(char op, const E& e)
		: op_(op), rhs_(make_ast_node_ptr(e))
	{}
	DEFINE_VISITABLE()

	char			op_;
	AstNodePtr		rhs_;
};


class Expr
	: public AstNode
{
public:
	Expr() {}
	DEFINE_VISITABLE()

	template<typename E>
	Expr(const E& e)
		: e_(make_ast_node_ptr(e))
	{}

	AstNodePtr e_;

	friend AstNodePtr make_ast_node_ptr(Expr const& t) { return t.e_; }
};


}
}
}
}
