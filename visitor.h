#pragma once


namespace logitech
{
namespace util
{


// Base class for visitors
class BaseVisitor
{
public:
	virtual ~BaseVisitor() {}
};


// Mixin to provide visitor properties to a visitor
template <class T, typename R = void>
class Visitor
{
public:
	typedef R ReturnType;
	virtual ReturnType Visit(T&) = 0;
};


// Base class for any class that can be visited. Parametrized on the visitor's return type.
template <class R = void>
class BaseVisitable
{
public:
	typedef R ReturnType;
	virtual ReturnType Accept(BaseVisitor&) = 0;

protected:
	template <class T>
	static ReturnType AcceptImpl(T& visitee, BaseVisitor& visitor)
	{
		if(Visitor<T, ReturnType>* p = dynamic_cast<Visitor<T, ReturnType>*>(&visitor))
		{
			return p->Visit(visitee);
		}
		return ReturnType();
	}
};

#define DEFINE_VISITABLE() \
	ReturnType Accept(logitech::util::BaseVisitor& visitor) override { return AcceptImpl(*this, visitor); }


}
}
