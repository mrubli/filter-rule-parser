/*
 * Grammar:
 * 
 * or_expr				::= and_expr [ 'or' or_expr ]
 * and_expr				::= factor [ 'and' factor ]
 * factor				::= term
 *						  | 'not' term
 * term					::= comparison_expr
 *						  | '(' and_expr ')'
 *
 * comparison_expr		::= variable comparison_op constant
 * comparison_op		::= '=' | '!=' | '<' | '>' | '<=' | '>='
 *
 * variable				::= identifier
 * constant				::= quoted_string
 *						  | hex_integer
 *						  | dec_integer
 *						  | real_number
 *						  | byte_list
 *
 * dec_integer			::= [ '-' ] digit { digit }
 * hex_integer			::= '0x' hexdigit { hexdigit }
 * real_number			::= dec_integer '.' digit { digit }
 * quoted_string		::= '"' { character } '"'
 * identifier			::= alpha_character { alpha_character } { '.' alpha_character { alpha_character } }
 * byte_list			::= '{' { { whitespace } hexdigit hexdigit } { whitespace } '}'
 */

#pragma warning(disable: 4100 4127)

#include <cstdint>
#include <iomanip>
#include <memory>
#include <string>
#include <vector>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/adapted.hpp>

#include "FilterRuleAst.h"


using namespace std;
using namespace logitech::uvcsnoop::filter::impl;

namespace qi = boost::spirit::qi;
namespace spirit = boost::spirit;
namespace phoenix = boost::phoenix;


namespace logitech
{
namespace uvcsnoop
{
namespace filter
{
namespace impl
{


struct MakeBinOp
{
	template<typename, typename, typename> struct result { typedef BinOp type; };

	template<typename S, typename L, typename R>
	BinOp operator() (const S& op, const L& lhs, const R& rhs) const 
	{
		return BinOp(op, lhs, rhs);
	}
};

struct MakeUnOp
{
	template<typename, typename> struct result { typedef UnOp type; };

	template<typename C, typename E>
	UnOp operator() (C op, const E& e) const 
	{
		return UnOp(op, e);
	}
};

phoenix::function<MakeBinOp> make_binop;
phoenix::function<MakeUnOp> make_unop;


template <typename Iterator>
struct Parser
	: qi::grammar<Iterator, Expr(), spirit::ascii::space_type>
{
	typedef spirit::ascii::space_type	skipper_type;

	Parser()
		: Parser::base_type(start)
	{
		using qi::_val;
		using qi::_1;
		using qi::_2;

		string				= qi::lexeme[+(qi::alpha)];
		identifier			= qi::lexeme[+(qi::alpha) >> *(qi::char_('.') >> +(qi::alpha))];
		byte				= qi::uint_parser<uint8_t, 16, 2, 2>();

		dec_integer			= qi::int_;
		hex_integer			= qi::lexeme[qi::lit("0x") >> qi::hex];
		real_number			= qi::real_parser<double, qi::strict_real_policies<double>>();
		quoted_string		= '"' >> *(qi::char_ - '"') >> '"';
		byte_list			= '{' >> *byte >> '}';

		variable			= identifier;
		constant			= hex_integer
							| real_number
							| dec_integer
							| quoted_string
							| byte_list;

		comparison_op		= qi::string("=")
							| qi::string("!=")
							| qi::string("<=")
							| qi::string(">=")
							| qi::string("<")
							| qi::string(">");
		comparison_expr		= variable							[ _val = _1 ]
							>> (comparison_op >> constant)		[ _val = make_binop(_1, _val, _2) ];

		term				= comparison_expr					[ _val = _1 ]
							| ('(' >> or_expr >> ')')			[ _val = _1 ];
		factor				= term								[ _val = _1 ]
							| qi::lit("not") >> term			[ _val = make_unop('!', _1) ];
		and_expr			= factor							[ _val = _1 ]
							>> *(qi::lit("and") >> factor)		[ _val = make_binop("&", _val, _1) ];
		or_expr				= and_expr							[ _val = _1 ]
							>> *(qi::lit("or") >> and_expr)		[ _val = make_binop("|", _val, _1) ];

		start = or_expr.alias();
	
		BOOST_SPIRIT_DEBUG_NODE(string);
		BOOST_SPIRIT_DEBUG_NODE(identifier);
		BOOST_SPIRIT_DEBUG_NODE(byte);
		BOOST_SPIRIT_DEBUG_NODE(dec_integer);
		BOOST_SPIRIT_DEBUG_NODE(hex_integer);
		BOOST_SPIRIT_DEBUG_NODE(real_number);
		BOOST_SPIRIT_DEBUG_NODE(byte_list);
		BOOST_SPIRIT_DEBUG_NODE(quoted_string);
		BOOST_SPIRIT_DEBUG_NODE(variable);
		BOOST_SPIRIT_DEBUG_NODE(constant);
		BOOST_SPIRIT_DEBUG_NODE(comparison_op);
		BOOST_SPIRIT_DEBUG_NODE(comparison_expr);
		BOOST_SPIRIT_DEBUG_NODE(term);
		BOOST_SPIRIT_DEBUG_NODE(factor);
		BOOST_SPIRIT_DEBUG_NODE(and_expr);
		BOOST_SPIRIT_DEBUG_NODE(or_expr);
	}

	qi::rule<Iterator, std::string(),			skipper_type>	string;
	qi::rule<Iterator, std::string(),			skipper_type>	identifier;
	qi::rule<Iterator, uint8_t(),				skipper_type>	byte;
	qi::rule<Iterator, int(),					skipper_type>	dec_integer;
	qi::rule<Iterator, unsigned int(),			skipper_type>	hex_integer;
	qi::rule<Iterator, double(),				skipper_type>	real_number;
	qi::rule<Iterator, std::vector<uint8_t>(),	skipper_type>	byte_list;
	qi::rule<Iterator, std::string(),			skipper_type>	quoted_string;
	qi::rule<Iterator, VarRef(),				skipper_type>	variable;
	qi::rule<Iterator, ConstVal(),				skipper_type>	constant;
	qi::rule<Iterator, std::string(),			skipper_type>	comparison_op;
	qi::rule<Iterator, Expr(),					skipper_type>	comparison_expr;
	qi::rule<Iterator, Expr(),					skipper_type>	term;
	qi::rule<Iterator, Expr(),					skipper_type>	factor;
	qi::rule<Iterator, Expr(),					skipper_type>	and_expr;
	qi::rule<Iterator, Expr(),					skipper_type>	or_expr;
	qi::rule<Iterator, Expr(),					skipper_type>	start;
};


template<typename Iterator>
bool
parse(Iterator first, Iterator last, Expr& ast)
{
	Parser<Iterator> grammar;

	bool success = qi::phrase_parse(first, last, grammar, qi::ascii::space, ast);
	if(first != last)
		return false;

	return success;
}


bool
parse(const string& input, Expr& ast)
{
	return parse(begin(input), end(input), ast);
}


}
}
}
}
