#pragma once

#include <memory>
#include <string>
#include <type_traits>
#include <vector>

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <noncopyable.h>


namespace logitech
{
namespace uvcsnoop
{
namespace filter
{


class Type
	: util::noncopyable
{
private:
	typedef std::shared_ptr<const Type> PtrType_;

public:
	static const PtrType_& ErrorType()  { static const auto t = std::make_shared<const Type>(); return t; }
	static const PtrType_& BoolType()   { static const auto t = std::make_shared<const Type>(); return t; }
	static const PtrType_& IntType()    { static const auto t = std::make_shared<const Type>(); return t; }
	static const PtrType_& UIntType()   { static const auto t = std::make_shared<const Type>(); return t; }
	static const PtrType_& DoubleType() { static const auto t = std::make_shared<const Type>(); return t; }
	static const PtrType_& StringType() { static const auto t = std::make_shared<const Type>(); return t; }
	static const PtrType_& RawType()    { static const auto t = std::make_shared<const Type>(); return t; }
};

typedef std::shared_ptr<const Type> TypePtr;


struct Symbol
	: util::noncopyable
{
public:
	Symbol(const std::string& name, const TypePtr& type)
		: Name(name), Type(type)
	{
	}

public:
	const std::string	Name;
	const TypePtr&		Type;
};

typedef std::shared_ptr<Symbol> SymbolPtr;


typedef boost::variant<
	int,
	unsigned int,
	double,
	std::string,
	std::vector<uint8_t>
> ValType;


struct IFilterEnvironment
{
	virtual ~IFilterEnvironment() {}

	virtual SymbolPtr					LookupVariable(const std::string& name) = 0;
	virtual boost::optional<ValType>	GetVariableValue(const SymbolPtr& symbol) = 0;
};

typedef std::shared_ptr<IFilterEnvironment> IFilterEnvironmentPtr;


class Rule
	: util::noncopyable
{
private:
	struct CheshireCat;

public:
	enum PrintFlags : unsigned int
	{
		PrintNone		= 0,
		PrintTypes		= 0x1,
		PrintValues		= 0x2,
	};
	typedef std::underlying_type<PrintFlags>::type PrintFlagsType;

private:
	Rule();

public:
	bool				IsValid() const;
	bool				Match() const;
	bool				IsSyntaxValid() const;
	std::string			ToString(PrintFlagsType printFlags = PrintNone) const;

private:
	std::unique_ptr<CheshireCat>	d_;		// Opaque pointer - Is it there? Is it not there?

	friend class FilterRuleParser;
};

typedef std::shared_ptr<Rule> RulePtr;


class FilterRuleParser
{
public:
	FilterRuleParser(const IFilterEnvironmentPtr& env);

	RulePtr				Parse(const std::string& input);

private:
	IFilterEnvironmentPtr	env_;
};


}
}
}
