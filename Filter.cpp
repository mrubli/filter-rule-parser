#include <functional>
#include <iomanip>
#include <map>
#include <ostream>
#include <sstream>
#include <string>

#include "Filter.h"
#include "FilterRuleAst.h"


using namespace std;
using namespace logitech::uvcsnoop;
using namespace logitech::uvcsnoop::filter;
using namespace logitech::uvcsnoop::filter::impl;


/******************************************************************************
 * EXTERNAL DECLARATIONS
 *****************************************************************************/

namespace logitech
{
namespace uvcsnoop
{
namespace filter
{
namespace impl
{

bool
parse(const string& input, Expr& ast);

}
}
}
}



/******************************************************************************
 * FUNCTIONS
 *****************************************************************************/

namespace logitech
{
namespace uvcsnoop
{
namespace filter
{


std::ostream&
operator<<(std::ostream& os, const TypePtr& t)
{
	if(t == Type::ErrorType())
		os << "Error";
	else if(t == Type::BoolType())
		os << "Bool";
	else if(t == Type::IntType())
		os << "Int";
	else if(t == Type::UIntType())
		os << "UInt";
	else if(t == Type::DoubleType())
		os << "Double";
	else if(t == Type::StringType())
		os << "String";
	else if(t == Type::RawType())
		os << "Raw";
	else
		os << "?";
	return os;
}


}
}
}



/******************************************************************************
 * TYPES
 *****************************************************************************/

namespace logitech
{
namespace uvcsnoop
{
namespace filter
{
namespace impl
{


class TreePrinter
	: util::noncopyable
	, public util::BaseVisitor
	, public util::Visitor<AstNode>
	, public util::Visitor<VarRef>
	, public util::Visitor<ConstVal>
	, public util::Visitor<BinOp>
	, public util::Visitor<UnOp>
	, public util::Visitor<Expr>
{
public:
	explicit TreePrinter(ostream& os, Rule::PrintFlagsType printFlags = Rule::PrintNone)
		: os_(os)
		, printFlags_(printFlags)
	{
	}

	void Visit(AstNode& e) override
	{
		Print(e);
	}

	void Visit(VarRef& e) override
	{
		PrintType(e.type_);
		PrintValue(e.result_);
		os_ << e.name_;
	}

	void Visit(ConstVal& e) override
	{
		struct ValuePrinter
			: public boost::static_visitor<string>
		{
			// Note: The to_string() functions below are broken in gcc on MinGW.
			//       See http://gcc.gnu.org/bugzilla/show_bug.cgi?id=52015 for details.
			string operator()(int i) const { return to_string(i); }
			string operator()(unsigned int u) const { return to_string(u); }
			string operator()(double d) const { return to_string(d); }
			string operator()(const string& s) const { return "\"" + s + "\""; }
			string operator()(const vector<uint8_t>& bytes) const
			{
				stringstream ss;
				ss << "[" << bytes.size() << "]"
					<< "{" << hex << setfill('0');
				for(auto b : bytes)
					ss << " " << setw(2) << (int)b;
				ss << " }";
				return ss.str();
			}
		};

		PrintType(e.type_);
		PrintValue(e.result_);
		os_ << boost::apply_visitor(ValuePrinter(), e.value_);
	}

	void Visit(BinOp& e) override
	{
		PrintType(e.type_);
		PrintValue(e.result_);
		os_ << "(";
		Print(*e.lhs_);
		os_ << " " << e.op_ << " ";
		Print(*e.rhs_);
		os_ << ")";
	}

	void Visit(UnOp& e) override
	{
		PrintType(e.type_);
		PrintValue(e.result_);
		os_ << e.op_;
		Print(*e.rhs_);
	}

	void Visit(Expr& e) override
	{
		Print(*e.e_);
	}

private:
	ostream&				os_;
	Rule::PrintFlagsType	printFlags_;

private:
	void Print(AstNode& e)
	{
		e.Accept(*this);
	}

	void PrintType(const TypePtr& type)
	{
		if(printFlags_ & Rule::PrintTypes)
		{
			if(type)
				os_ << "[" << type << "]";
		}
	}

	void PrintValue(const boost::optional<bool>& value)
	{
		if(printFlags_ & Rule::PrintValues)
		{
			if(value)
			{
				os_ << "|" << (value.get() ? "true" : "false") << "|";
			}
			else
			{
				//os_ << "|?|";
			}
		}
	}
};


class TreeAnalyzer
	: util::noncopyable
	, public util::BaseVisitor
	, public util::Visitor<AstNode>
	, public util::Visitor<VarRef>
	, public util::Visitor<ConstVal>
	, public util::Visitor<BinOp>
	, public util::Visitor<UnOp>
	, public util::Visitor<Expr>
{
public:
	explicit TreeAnalyzer(const IFilterEnvironmentPtr& env)
		: env_(env)
	{
	}

	void Visit(AstNode& e) override
	{
		Check(e);
	}

	void Visit(VarRef& e) override
	{
		e.symbol_ = env_->LookupVariable(e.name_);
		if(e.symbol_)
		{
			e.type_ = e.symbol_->Type;
		}
		else
		{
			e.type_ = Type::ErrorType();
		}
	}

	void Visit(ConstVal& e) override
	{
		struct ValuePrinter
			: public boost::static_visitor<TypePtr>
		{
			TypePtr operator()(int) const { return Type::IntType(); }
			TypePtr operator()(unsigned int) const { return Type::UIntType(); }
			TypePtr operator()(double) const { return Type::DoubleType(); }
			TypePtr operator()(const string&) const { return Type::StringType(); }
			TypePtr operator()(const vector<uint8_t>&) const { return Type::RawType(); }
		};

		e.type_= boost::apply_visitor(ValuePrinter(), e.value_);
	}

	void Visit(BinOp& e) override
	{
		Check(*e.lhs_);
		Check(*e.rhs_);

		const auto ltype = e.lhs_->type_;
		const auto rtype = e.rhs_->type_;
		if(ltype == Type::ErrorType() || rtype == Type::ErrorType())
		{
			e.type_ = Type::ErrorType();
		}
		else if(ltype != rtype)
		{
			e.type_ = Type::ErrorType();
		}
		else
		{
			e.type_ = Type::BoolType();
		}
	}

	void Visit(UnOp& e) override
	{
		Check(*e.rhs_);

		const auto rtype = e.rhs_->type_;
		if(rtype == Type::ErrorType())
		{
			e.type_ = Type::ErrorType();
		}
		else if(rtype != Type::BoolType())
		{
			e.type_ = Type::ErrorType();
		}
		else
		{
			e.type_ = Type::BoolType();
		}
	}

	void Visit(Expr& e) override
	{
		Check(*e.e_);
		e.type_ = e.e_->type_;
	}

private:
	IFilterEnvironmentPtr	env_;

private:
	void Check(AstNode& e)
	{
		e.Accept(*this);
	}
};


class FilterRuleTreeEvaluator
	: util::noncopyable
	, public util::BaseVisitor
	, public util::Visitor<AstNode>
	, public util::Visitor<VarRef>
	, public util::Visitor<ConstVal>
	, public util::Visitor<BinOp>
	, public util::Visitor<UnOp>
	, public util::Visitor<Expr>
{
public:
	explicit FilterRuleTreeEvaluator(const IFilterEnvironmentPtr& env)
		: env_(env)
	{
	}

	void Visit(AstNode& e) override
	{
		Eval(e);
	}

	void Visit(VarRef&) override
	{
	}

	void Visit(ConstVal&) override
	{
	}

	void Visit(BinOp& e) override
	{
		static map<string, function<bool(ValType, ValType)>> compOps;
		if(compOps.empty())
		{
			compOps["="]  = equal_to<ValType>();
			compOps["!="] = not2(equal_to<ValType>());
			compOps["<"]  = less<ValType>();
			compOps[">"]  = [] (const ValType& a, const ValType& b) { return !(a < b) && !(a == b); };
			compOps["<="]  = [] (const ValType& a, const ValType& b) { return (a < b) || (a == b); };
			compOps[">="] = not2(less<ValType>());
		}

		if(compOps.find(e.op_) != compOps.end())
		{
			// Comparison operation (between VarRef and ConstVal)
			const auto& symbol = static_cast<VarRef&>(*e.lhs_).symbol_;
			const auto& varValue = env_->GetVariableValue(symbol);
			const auto& constValue = static_cast<ConstVal&>(*e.rhs_).value_;

			if(varValue)
			{
				e.result_ = compOps.at(e.op_)(varValue.get(), constValue);
			}
			else
			{
				e.result_ = false;
			}
		}
		else
		{
			// Binary logical operation (between two Boolean values)

			// Evaluate the left-hand side first
			Eval(*e.lhs_);
			const bool lval = e.lhs_->result_.get();

			if(e.op_ == "&" && lval == false)		// Short-circuiting for AND
			{
				e.result_ = false;
			}
			else if(e.op_ == "|" && lval == true)	// Short-circuiting for OR
			{
				e.result_ = true;
			}
			else
			{
				// Evaluate the right-hand side
				Eval(*e.rhs_);
				const bool rval = e.lhs_->result_.get();

				if(e.op_ == "&")
				{
					e.result_ = lval && rval;
				}
				else
				{
					e.result_ = lval || rval;
				}
			}
		}
	}

	void Visit(UnOp& e) override
	{
		static map<char, function<bool(bool)>> ops;
		if(ops.empty())
		{
			ops['!'] = logical_not<bool>();
		}

		Eval(*e.rhs_);
		const bool rval = e.rhs_->result_.get();

		e.result_ = ops.at(e.op_)(rval);
	}

	void Visit(Expr& e) override
	{
		Eval(*e.e_);
		e.result_ = e.e_->result_.get();
	}

private:
	IFilterEnvironmentPtr	env_;

private:
	void Eval(AstNode& e)
	{
		e.Accept(*this);
	}
};


}
}
}
}



/******************************************************************************
 * Rule
 *****************************************************************************/

// Private implementation for Rule class
struct logitech::uvcsnoop::filter::Rule::CheshireCat
{
	std::string				Input;		// Original input string
	IFilterEnvironmentPtr	Env;		// Environment
	AstNodePtr				Expr;		// Expression tree (empty unless parsing succeeded)

	void Init(const std::string& input, const IFilterEnvironmentPtr& env)
	{
		Input = input;
		Env = env;

		// Try to parse the input
		auto expr = make_shared<impl::Expr>();
		bool parsed = impl::parse(input, *expr);
		if(parsed)
		{
			// Save the expression tree if parsing was successful
			Expr = expr;

			// Type check the expression
			TreeAnalyzer analyzer(Env);
			analyzer.Visit(*Expr);
		}
	}
};


logitech::uvcsnoop::filter::Rule::Rule()
	: d_(new CheshireCat)
{
}


// Returns true if type checking was successful.
bool
logitech::uvcsnoop::filter::Rule::IsValid() const
{
	if(d_->Expr)
	{
		assert(d_->Expr->type_);
		if(d_->Expr->type_ != Type::ErrorType())
		{
			assert(d_->Expr->type_ == Type::BoolType());
			return true;
		}
	}

	return false;
}


bool
logitech::uvcsnoop::filter::Rule::Match() const
{
	assert(IsValid());

	if(IsValid())
	{
		FilterRuleTreeEvaluator evaluator(d_->Env);
		evaluator.Visit(*d_->Expr);

		assert(d_->Expr->result_);
		if(d_->Expr->result_)
		{
			return d_->Expr->result_.get();
		}
	}

	return false;
}


// Returns true if syntax checking was successful;
bool
logitech::uvcsnoop::filter::Rule::IsSyntaxValid() const
{
	return d_->Expr;	// Syntax is valid if we have an expression tree
}


std::string
logitech::uvcsnoop::filter::Rule::ToString(PrintFlagsType printFlags) const
{
	stringstream ss;
	TreePrinter printer(ss, printFlags);
	printer.Visit(*d_->Expr);
	return ss.str();
}



/******************************************************************************
 * FilterRuleParser
 *****************************************************************************/

logitech::uvcsnoop::filter::FilterRuleParser::FilterRuleParser(const IFilterEnvironmentPtr& env)
	: env_(env)
{
}


RulePtr
logitech::uvcsnoop::filter::FilterRuleParser::Parse(const std::string& input)
{
	RulePtr filterRule(new Rule);
	filterRule->d_->Init(input, env_);

	return filterRule;
}
