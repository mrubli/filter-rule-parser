#pragma once


namespace logitech
{
namespace util
{


class noncopyable
{
protected:
	noncopyable() {}
	~noncopyable() {}

private:
	noncopyable(const noncopyable&);				// Not implemented
	noncopyable& operator=(const noncopyable&);		// Not implemented
};


}
}
